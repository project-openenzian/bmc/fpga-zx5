-- Copyright (c) 2022, Enclustra GmbH, Switzerland
-- Copyright (c) 2023, ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;


entity Enzian_BMC_ZX5 is
    port (
        --------------------
        -- Mercury Module --
        --------------------
        -- LED
        ZX5_LED0_N            : out std_logic;

        -- DRAM
        DDR_addr            : inout std_logic_vector (14 downto 0);
        DDR_ba              : inout std_logic_vector (2 downto 0);
        DDR_cas_n           : inout std_logic;
        DDR_ck_n            : inout std_logic;
        DDR_ck_p            : inout std_logic;
        DDR_cke             : inout std_logic;
        DDR_cs_n            : inout std_logic;
        DDR_dm              : inout std_logic_vector (3 downto 0);
        DDR_dq              : inout std_logic_vector (31 downto 0);
        DDR_dqs_n           : inout std_logic_vector (3 downto 0);
        DDR_dqs_p           : inout std_logic_vector (3 downto 0);
        DDR_odt             : inout std_logic;
        DDR_ras_n           : inout std_logic;
        DDR_reset_n         : inout std_logic;
        DDR_we_n            : inout std_logic;
        
        -- Fixed I/O
        FIXED_IO_ddr_vrn    : inout std_logic;
        FIXED_IO_ddr_vrp    : inout std_logic;
        FIXED_IO_mio        : inout std_logic_vector (53 downto 0);
        FIXED_IO_ps_clk     : inout std_logic;
        FIXED_IO_ps_porb    : inout std_logic;
        FIXED_IO_ps_srstb   : inout std_logic;

        -- On-module I2C bus
        ZX5_I2C_SDA         : inout std_logic;
        ZX5_I2C_SCL         : inout std_logic;

        ------------
        -- Enzian --
        ------------
        -- Power/Fan I2C and pins
        B_PWR_FAN_I2C_SDA   : inout std_logic;
        B_PWR_FAN_I2C_SCL   : inout std_logic;
        B_PWR_ALERT_N       : in std_logic;
        B_FAN_RESET_N       : out std_logic;
        B_FAN_ALERT_N       : in std_logic;
        B_FAN_FAULT_N       : inout std_logic;
        
        -- Power sequencer (isppac) I2C bus and alert signal
        B_SEQ_I2C_SDA       : inout std_logic;
        B_SEQ_I2C_SCL       : inout std_logic;
        B_SEQ_ALERT_N       : in std_logic;
        
        -- PSU I2C bus and alert signal
        B_PSU_I2C_SDA       : inout std_logic;
        B_PSU_I2C_SCL       : inout std_logic;
        B_PSU_ALERT_N       : in std_logic;
        
        -- Clock generator I2C bus
        B_CLOCK_I2C_SDA     : inout std_logic;
        B_CLOCK_I2C_SCL     : inout std_logic;
        
        -- Fornt-panel IO
        B_FPIO_PWR_LED      : out std_logic;
        B_FPIO_PWR_SW_N     : in std_logic;
        B_FPIO_RST_SW_N     : in std_logic;
        B_FPIO_SID_SW_N     : in std_logic;
        B_FPIO_1WIRE        : inout std_logic;
        B_FPIO_NMI_SW_N     : in std_logic;
        B_FPIO_SID_LED      : out std_logic;
        B_FPIO_F1_LED       : out std_logic;
        B_FPIO_F2_LED       : out std_logic;
        B_FPIO_NIC1_LED     : out std_logic;
        B_FPIO_I2C_SDA      : inout std_logic;
        B_FPIO_I2C_SCL      : inout std_logic;
        B_FPIO_CI_SW_N      : in std_logic;
        B_FPIO_NIC2_LED     : out std_logic;

        -- Enzian USB UARTs
        B_UART0_RXD         : in std_logic;
        B_UART0_TXD         : out std_logic;
        B_UART0_CTS         : in STD_LOGIC;
        B_UART0_RTS         : out STD_LOGIC;
        
        B_UART1_RXD         : in std_logic;
        B_UART1_TXD         : out std_logic;
        B_UART1_CTS         : in STD_LOGIC;
        B_UART1_RTS         : out STD_LOGIC;
        
        B_UART2_RXD         : in std_logic;
        B_UART2_TXD         : out std_logic;
        B_UART2_CTS         : in STD_LOGIC;
        B_UART2_RTS         : out STD_LOGIC;
        
        B_UART3_RXD         : in std_logic;
        B_UART3_TXD         : out std_logic;
        B_UART3_CTS         : in STD_LOGIC;
        B_UART3_RTS         : out STD_LOGIC;

        -- UARTs to CPU (ThunderX)
        B_CUART0_RXD        : in STD_LOGIC;
        B_CUART0_TXD        : out STD_LOGIC;
        B_CUART0_CTS        : in STD_LOGIC;
        B_CUART0_RTS        : out STD_LOGIC;

        B_CUART1_RXD        : in STD_LOGIC;
        B_CUART1_TXD        : out STD_LOGIC;
        B_CUART1_CTS        : in STD_LOGIC;
        B_CUART1_RTS        : out STD_LOGIC;

        -- UART to FPGA (XCVU9P)
        B_FUART_RXD         : in STD_LOGIC;
        B_FUART_TXD         : out STD_LOGIC;
        B_FUART_CTS         : in STD_LOGIC;
        B_FUART_RTS         : out STD_LOGIC;
        
        -- CPU DRAM voltage control (parallel VID)
        B_CDV_VID           : out std_logic_vector (7 downto 0);

        -- FPGA (XCVU9P) DRAM voltage control (parallel VID)
        B_FDV_VID           : out std_logic_vector (7 downto 0);

        -- BMC user LEDs on baseboard
        B_USERIO_LED        : out std_logic_vector (8 downto 1);
        
        -- BMC user IO DIP switches on baseboard
        B_USERIO_SW_N       : in std_logic_vector (8 downto 1);
        
        -- CPU SPI boot flash
        B_SPI_SCK           : out std_logic;
        B_SPI_CS_N          : out std_logic;
        B_SPI_MOSI          : out std_logic;
        B_SPI_MISO          : in std_logic;
        B_SPI_SEL_N         : out std_logic;

        -- FPGA (XCVU9P) serial configuration interface
        F_SSCONF_PROG_B     : out std_logic;
        F_SSCONF_DATA       : out std_logic;
        F_SSCONF_CLK        : out std_logic;
        F_SSCONF_INIT_B     : in std_logic;
        F_SSCONF_DONE       : in std_logic;
        
        -- PSU control pin
        B_PSUP_ON           : out std_logic;

        -- CPU pinstraps and reset control
        C_RESET_OUT_N       : in std_logic;
        C_RESET_N           : out std_logic;
        C_PLL_DCOK          : out std_logic;
        B_OCI2_LNK1         : out std_logic;
        B_OCI3_LNK1         : out std_logic;
        
        -- Clock generator Loss-Of-Lock signals 
        B_CLOCK_BLOL        : in std_logic;
        B_CLOCK_CLOL        : in std_logic;
        B_CLOCK_FLOL        : in std_logic;
        
        -- FMC clock source selector
        B_FMCC_SEL          : out std_logic;
        
        -- Interrupt pin from FPGA (XCVU9P)
        B_C2C_NMI           : out std_logic;
        
        -- USB2 VBUS enable and overcurrent detect
        B_USB2_EN           : out std_logic;
        B_USB2_OC_N         : in std_logic;
        
        -- CPU NCSI (Ethernet sideband interface)
        B_NCSI_CRS_DV       : in std_logic;
        B_NCSI_RX_ER        : in std_logic;
        B_NCSI_RXD          : in std_logic_vector(1 downto 0);
        B_NCSI_TX_EN        : out std_logic;
        B_NCSI_TXD          : out std_logic_vector(1 downto 0);
        B_NCSIC             : in std_logic;

        -- CPU and FPGA DRAM ALERT signals
        F_D_EVENT_N         : inout std_logic;
        C_D_EVENT_N         : inout std_logic;
        
        -- Chip-to-chip link between the BMC and the FPGA
        MGT_REFCLK0_P       : in std_ulogic; 
        MGT_REFCLK0_N       : in std_ulogic; 
        MGT_REFCLK1_P       : in std_ulogic; 
        MGT_REFCLK1_N       : in std_ulogic; 
                
        MGT_TX_P           : out std_logic_vector(3 downto 0);
        MGT_TX_N           : out std_logic_vector(3 downto 0); 
        MGT_RX_P           : in std_logic_vector(3 downto 0);
        MGT_RX_N           : in std_logic_vector(3 downto 0)
    );
end Enzian_BMC_ZX5;

architecture rtl of Enzian_BMC_ZX5 is
    component Mercury_ZX5
        port (
            -- From PS
            Clk100 : out STD_LOGIC;
            RESET_N : out STD_LOGIC;

            ZX5_I2C_scl_i : in STD_LOGIC;
            ZX5_I2C_scl_o : out STD_LOGIC;
            ZX5_I2C_scl_t : out STD_LOGIC;
            ZX5_I2C_sda_i : in STD_LOGIC;
            ZX5_I2C_sda_o : out STD_LOGIC;
            ZX5_I2C_sda_t : out STD_LOGIC;

            -- GPIO_LED_N
            GPIO_LED_N : out STD_LOGIC_VECTOR(0 downto 0);

            -- DRAM
            DDR_addr : inout STD_LOGIC_VECTOR (14 downto 0);
            DDR_ba : inout STD_LOGIC_VECTOR (2 downto 0);
            DDR_cas_n : inout STD_LOGIC;
            DDR_ck_n : inout STD_LOGIC;
            DDR_ck_p : inout STD_LOGIC;
            DDR_cke : inout STD_LOGIC;
            DDR_cs_n : inout STD_LOGIC;
            DDR_dm : inout STD_LOGIC_VECTOR (3 downto 0);
            DDR_dq : inout STD_LOGIC_VECTOR (31 downto 0);
            DDR_dqs_n : inout STD_LOGIC_VECTOR (3 downto 0);
            DDR_dqs_p : inout STD_LOGIC_VECTOR (3 downto 0);
            DDR_odt : inout STD_LOGIC;
            DDR_ras_n : inout STD_LOGIC;
            DDR_reset_n : inout STD_LOGIC;
            DDR_we_n : inout STD_LOGIC;

            -- Fixed I/O
            FIXED_IO_ddr_vrn : inout STD_LOGIC;
            FIXED_IO_ddr_vrp : inout STD_LOGIC;
            FIXED_IO_mio : inout STD_LOGIC_VECTOR (53 downto 0);
            FIXED_IO_ps_clk : inout STD_LOGIC;
            FIXED_IO_ps_porb : inout STD_LOGIC;
            FIXED_IO_ps_srstb : inout STD_LOGIC;

            -- GPIO_I2C
            GPIO_I2C_tri_i : in STD_LOGIC_VECTOR (9 downto 0);
            GPIO_I2C_tri_o : out STD_LOGIC_VECTOR (9 downto 0);
            GPIO_I2C_tri_t : out STD_LOGIC_VECTOR (9 downto 0);

            -- GPIO_0
            GPIO_0_tri_i : in STD_LOGIC_VECTOR (31 downto 0);
            GPIO_0_tri_o : out STD_LOGIC_VECTOR (31 downto 0);
            GPIO_0_tri_t : out STD_LOGIC_VECTOR (31 downto 0);

            -- GPIO_1
            GPIO_1_tri_i : in STD_LOGIC_VECTOR (31 downto 0);
            GPIO_1_tri_o : out STD_LOGIC_VECTOR (31 downto 0);
            GPIO_1_tri_t : out STD_LOGIC_VECTOR (31 downto 0);

            -- ENZIAN_UART_0
            ENZIAN_UART_0_rxd : in STD_LOGIC;
            ENZIAN_UART_0_txd : out STD_LOGIC;
            ENZIAN_UART_0_ctsn : in STD_LOGIC;
            ENZIAN_UART_0_rtsn : out STD_LOGIC;
            
            -- ENZIAN_UART_1
            ENZIAN_UART_1_rxd : in STD_LOGIC;
            ENZIAN_UART_1_txd : out STD_LOGIC;
            ENZIAN_UART_1_ctsn : in STD_LOGIC;
            ENZIAN_UART_1_rtsn : out STD_LOGIC;
            
            -- ENZIAN_UART_2
            ENZIAN_UART_2_rxd : in STD_LOGIC;
            ENZIAN_UART_2_txd : out STD_LOGIC;
            ENZIAN_UART_2_ctsn : in STD_LOGIC;
            ENZIAN_UART_2_rtsn : out STD_LOGIC;
            
            -- ENZIAN_UART_3
            ENZIAN_UART_3_rxd : in STD_LOGIC;
            ENZIAN_UART_3_txd : out STD_LOGIC;
            ENZIAN_UART_3_ctsn : in STD_LOGIC;
            ENZIAN_UART_3_rtsn : out STD_LOGIC;

            -- CPU_UART_0
            CPU_UART_0_rxd : in STD_LOGIC;
            CPU_UART_0_txd : out STD_LOGIC;
            CPU_UART_0_ctsn : in STD_LOGIC;
            CPU_UART_0_rtsn : out STD_LOGIC;

            -- CPU_UART_1
            CPU_UART_1_rxd : in STD_LOGIC;
            CPU_UART_1_txd : out STD_LOGIC;
            CPU_UART_1_ctsn : in STD_LOGIC;
            CPU_UART_1_rtsn : out STD_LOGIC;

            -- FPGA_UART
            FPGA_UART_rxd : in STD_LOGIC;
            FPGA_UART_txd : out STD_LOGIC;
            FPGA_UART_ctsn : in STD_LOGIC;
            FPGA_UART_rtsn : out STD_LOGIC;

            -- FPGA_SSCONF
            FPGA_SSCONF_io0_o : out STD_LOGIC;
            FPGA_SSCONF_sck_o : out STD_LOGIC;

            -- ENZIAN_FLASH_SPI
            ENZIAN_FLASH_SPI_io0_o : out STD_LOGIC;
            ENZIAN_FLASH_SPI_io1_i : in STD_LOGIC;
            ENZIAN_FLASH_SPI_sck_o : out STD_LOGIC;
            ENZIAN_FLASH_SPI_ss_o : out STD_LOGIC_VECTOR (0 to 0);

            -- NCSI_MII
            NCSI_MII_col : in STD_LOGIC;
            NCSI_MII_crs : in STD_LOGIC;
            NCSI_MII_rst_n : out STD_LOGIC;
            NCSI_MII_rx_clk : in STD_LOGIC;
            NCSI_MII_rx_dv : in STD_LOGIC;
            NCSI_MII_rx_er : in STD_LOGIC;
            NCSI_MII_rxd : in STD_LOGIC_VECTOR (3 downto 0);
            NCSI_MII_tx_clk : in STD_LOGIC;
            NCSI_MII_tx_en : out STD_LOGIC;
            NCSI_MII_txd : out STD_LOGIC_VECTOR (3 downto 0);

            -- GPIO_CDV
            GPIO_CDV_tri_o : out STD_LOGIC_VECTOR (7 downto 0);

            -- GPIO_FDV
            GPIO_FDV_tri_o : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component Mercury_ZX5;

    component mii_to_rmii
        port (
            rst_n : in STD_LOGIC;
            ref_clk : in STD_LOGIC;
            mac2rmii_tx_en : in STD_LOGIC;
            mac2rmii_txd : in STD_LOGIC_VECTOR (3 downto 0);
            mac2rmii_tx_er : in STD_LOGIC;
            rmii2mac_tx_clk : out STD_LOGIC;
            rmii2mac_rx_clk : out STD_LOGIC;
            rmii2mac_col : out STD_LOGIC;
            rmii2mac_crs : out STD_LOGIC;
            rmii2mac_rx_dv : out STD_LOGIC;
            rmii2mac_rx_er : out STD_LOGIC;
            rmii2mac_rxd : out STD_LOGIC_VECTOR (3 downto 0);
            phy2rmii_crs_dv : in STD_LOGIC;
            phy2rmii_rx_er : in STD_LOGIC;
            phy2rmii_rxd : in STD_LOGIC_VECTOR (1 downto 0);
            rmii2phy_txd : out STD_LOGIC_VECTOR (1 downto 0);
            rmii2phy_tx_en : out STD_LOGIC
        );
    end component mii_to_rmii;
    
    component ibert_7series_gtp_0
        port (
            TXN_O : out STD_LOGIC_VECTOR (3 downto 0);
            TXP_O : out STD_LOGIC_VECTOR (3 downto 0);
            RXOUTCLK_O : out STD_LOGIC;
            RXN_I : in STD_LOGIC_VECTOR (3 downto 0);
            RXP_I : in STD_LOGIC_VECTOR (3 downto 0);
            GTREFCLK0_I : in STD_LOGIC_VECTOR (0 downto 0);
            GTREFCLK1_I : in STD_LOGIC_VECTOR (0 downto 0);
            SYSCLK_I : in STD_LOGIC
        );
    end component ibert_7series_gtp_0;

    signal Clk100 : std_logic;

    signal ZX5_I2C_sda_i : std_logic;
    signal ZX5_I2C_sda_o : std_logic;
    signal ZX5_I2C_sda_t : std_logic;
    signal ZX5_I2C_scl_o : std_logic;
    signal ZX5_I2C_scl_i : std_logic;
    signal ZX5_I2C_scl_t : std_logic;

    signal GPIO_LED_N     : std_logic_vector (0 downto 0);

    signal GPIO_0_tri_i : std_logic_vector (31 downto 0);
    signal GPIO_0_tri_o : std_logic_vector (31 downto 0);
    signal GPIO_0_tri_t : std_logic_vector (31 downto 0);
    signal GPIO_1_tri_i : std_logic_vector (31 downto 0);
    signal GPIO_1_tri_o : std_logic_vector (31 downto 0);
    signal GPIO_1_tri_t : std_logic_vector (31 downto 0);

    signal GPIO_I2C_tri_i : STD_LOGIC_VECTOR (9 downto 0);
    signal GPIO_I2C_tri_o : STD_LOGIC_VECTOR (9 downto 0);
    signal GPIO_I2C_tri_t : STD_LOGIC_VECTOR (9 downto 0);

    signal FAN_FAULT_N_tri_i : STD_LOGIC;
    signal FAN_FAULT_N_tri_o : STD_LOGIC;
    signal FAN_FAULT_N_tri_t : STD_LOGIC;

    signal C_D_EVENT_N_tri_i : std_logic;
    signal C_D_EVENT_N_tri_o : std_logic;
    signal C_D_EVENT_N_tri_t : std_logic;

    signal F_D_EVENT_N_tri_i : std_logic;
    signal F_D_EVENT_N_tri_o : std_logic;
    signal F_D_EVENT_N_tri_t : std_logic;

    signal F_FPIO_1WIRE_tri_i: STD_LOGIC;
    signal F_FPIO_1WIRE_tri_o: STD_LOGIC;
    signal F_FPIO_1WIRE_tri_t: STD_LOGIC;

    signal NCSI_MII_col : STD_LOGIC;
    signal NCSI_MII_crs : STD_LOGIC;
    signal NCSI_MII_rst_n : STD_LOGIC;
    signal NCSI_MII_rx_clk : STD_LOGIC;
    signal NCSI_MII_rx_dv : STD_LOGIC;
    signal NCSI_MII_rx_er : STD_LOGIC;
    signal NCSI_MII_rxd : STD_LOGIC_VECTOR(3 downto 0);
    signal NCSI_MII_tx_clk : STD_LOGIC;
    signal NCSI_MII_tx_en : STD_LOGIC;
    signal NCSI_MII_txd : STD_LOGIC_VECTOR(3 downto 0);

    signal RXOUTCLK_O : STD_LOGIC;
    signal GTREFCLK0_I : STD_LOGIC_VECTOR (0 downto 0);
    signal GTREFCLK1_I : STD_LOGIC_VECTOR (0 downto 0);

begin
    i_Mercury_ZX5 : Mercury_ZX5
        port map (
            Clk100 => Clk100,

            ZX5_I2C_sda_i => ZX5_I2C_sda_i,
            ZX5_I2C_sda_o => ZX5_I2C_sda_o,
            ZX5_I2C_sda_t => ZX5_I2C_sda_t,
            ZX5_I2C_scl_i => ZX5_I2C_scl_i,
            ZX5_I2C_scl_o => ZX5_I2C_scl_o,
            ZX5_I2C_scl_t => ZX5_I2C_scl_t,

            GPIO_LED_N => GPIO_LED_N,

            DDR_addr			=> DDR_addr,
            DDR_ba				=> DDR_ba,
            DDR_cas_n			=> DDR_cas_n,
            DDR_ck_n			=> DDR_ck_n,
            DDR_ck_p			=> DDR_ck_p,
            DDR_cke				=> DDR_cke,
            DDR_cs_n			=> DDR_cs_n,
            DDR_dm				=> DDR_dm,
            DDR_dq				=> DDR_dq,
            DDR_dqs_n			=> DDR_dqs_n,
            DDR_dqs_p			=> DDR_dqs_p,
            DDR_odt				=> DDR_odt,
            DDR_ras_n			=> DDR_ras_n,
            DDR_reset_n			=> DDR_reset_n,
            DDR_we_n			=> DDR_we_n,

            FIXED_IO_ddr_vrn	=> FIXED_IO_ddr_vrn,
            FIXED_IO_ddr_vrp	=> FIXED_IO_ddr_vrp,
            FIXED_IO_mio		=> FIXED_IO_mio,
            FIXED_IO_ps_clk		=> FIXED_IO_ps_clk,
            FIXED_IO_ps_porb	=> FIXED_IO_ps_porb,
            FIXED_IO_ps_srstb	=> FIXED_IO_ps_srstb,

            GPIO_0_tri_i    => GPIO_0_tri_i,
            GPIO_0_tri_o    => GPIO_0_tri_o,
            GPIO_0_tri_t    => GPIO_0_tri_t,
            GPIO_1_tri_i    => GPIO_1_tri_i,
            GPIO_1_tri_o    => GPIO_1_tri_o,
            GPIO_1_tri_t    => GPIO_1_tri_t,

            GPIO_I2C_tri_i => GPIO_I2C_tri_i,
            GPIO_I2C_tri_o => GPIO_I2C_tri_o,
            GPIO_I2C_tri_t => GPIO_I2C_tri_t,

            GPIO_CDV_tri_o      => B_CDV_VID,
            GPIO_FDV_tri_o      => B_FDV_VID,

            -- Some B_UARTs (ENZIAN_UARTs) are actually routed to CPU_UARTs/FPGA_UART below
            -- The UART controllers and these routing rules are preserved in case these
            -- ports needs to be monitored/driven by the BMC via software.
            
            -- USB UART0 routed in block diagram
            ENZIAN_UART_0_rxd => B_UART0_RXD,
            ENZIAN_UART_0_txd => B_UART0_TXD,
            ENZIAN_UART_0_ctsn => B_UART0_CTS,
            ENZIAN_UART_0_rtsn => B_UART0_RTS,

            -- USB UART1 routed directly below
            ENZIAN_UART_1_ctsn => '1',
            -- ENZIAN_UART_1_rtsn => ,
            ENZIAN_UART_1_rxd => '0',
            -- ENZIAN_UART_1_txd => ,

            -- USB UART1 routed in block diagram
            ENZIAN_UART_2_ctsn => B_UART2_CTS,
            ENZIAN_UART_2_rtsn => B_UART2_RTS,
            ENZIAN_UART_2_rxd => B_UART2_RXD,
            ENZIAN_UART_2_txd => B_UART2_TXD,

            -- USB UART2 routed directly below
            ENZIAN_UART_3_ctsn => '1',
            -- ENZIAN_UART_3_rtsn => ,
            ENZIAN_UART_3_rxd => '0',
            -- ENZIAN_UART_2_txd => ,

            -- CPU UART0 routed directly below
            CPU_UART_0_ctsn => '1',
            -- CPU_UART_0_rtsn => ,
            CPU_UART_0_rxd => '0',
            -- CPU_UART_0_txd => ,

            -- CPU UART1 routed in block diagram
            CPU_UART_1_ctsn => B_CUART1_CTS,
            CPU_UART_1_rtsn => B_CUART1_RTS,
            CPU_UART_1_rxd => B_CUART1_RXD,
            CPU_UART_1_txd => B_CUART1_TXD,

            -- FPGA UART routed directly below
            FPGA_UART_ctsn => '1',
            -- FPGA_UART_rtsn => ,
            FPGA_UART_rxd => '0',
            -- FPGA_UART_txd => ,

            FPGA_SSCONF_io0_o => F_SSCONF_DATA,
            FPGA_SSCONF_sck_o => F_SSCONF_CLK,

            ENZIAN_FLASH_SPI_io0_o => B_SPI_MOSI,
            ENZIAN_FLASH_SPI_io1_i => B_SPI_MISO,
            ENZIAN_FLASH_SPI_sck_o => B_SPI_SCK,
            ENZIAN_FLASH_SPI_ss_o(0) => B_SPI_CS_N,
            
            NCSI_MII_col => NCSI_MII_col,
            NCSI_MII_crs => NCSI_MII_crs,
            NCSI_MII_rst_n => NCSI_MII_rst_n,
            NCSI_MII_rx_clk => NCSI_MII_rx_clk,
            NCSI_MII_rx_dv => NCSI_MII_rx_dv,
            NCSI_MII_rx_er => NCSI_MII_rx_er,
            NCSI_MII_rxd => NCSI_MII_rxd,
            NCSI_MII_tx_clk => NCSI_MII_tx_clk,
            NCSI_MII_tx_en => NCSI_MII_tx_en,
            NCSI_MII_txd => NCSI_MII_txd
        );

    ZX5_LED0_N <= '0' when GPIO_LED_N(0) = '0' else 'Z';

    -- IOBUFs
    -- NOTICE: I to tri_o and O to tri_i
    ZX5_I2C_SDA_iobuf : IOBUF
        port map (
            I => ZX5_I2C_sda_o,
            O => ZX5_I2C_sda_i,
            T => ZX5_I2C_sda_t,
            IO => ZX5_I2C_SDA
        );
    ZX5_I2C_SCL_iobuf : IOBUF
        port map (
            I => ZX5_I2C_scl_o,
            O => ZX5_I2C_scl_i,
            T => ZX5_I2C_scl_t,
            IO => ZX5_I2C_SCL
        );
    SEQ_I2C_SDA_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(0),
            O => GPIO_I2C_tri_i(0),
            T => GPIO_I2C_tri_t(0),
            IO => B_SEQ_I2C_SDA
        );
    SEQ_I2C_SCL_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(1),
            O => GPIO_I2C_tri_i(1),
            T => GPIO_I2C_tri_t(1),
            IO => B_SEQ_I2C_SCL
        );
    PSU_I2C_SDA_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(2),
            O => GPIO_I2C_tri_i(2),
            T => GPIO_I2C_tri_t(2),
            IO => B_PSU_I2C_SDA
        );
    PSU_I2C_SCL_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(3),
            O => GPIO_I2C_tri_i(3),
            T => GPIO_I2C_tri_t(3),
            IO => B_PSU_I2C_SCL
        );
    CLOCK_I2C_SDA_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(4),
            O => GPIO_I2C_tri_i(4),
            T => GPIO_I2C_tri_t(4),
            IO => B_CLOCK_I2C_SDA
        );
    CLOCK_I2C_SCL_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(5),
            O => GPIO_I2C_tri_i(5),
            T => GPIO_I2C_tri_t(5),
            IO => B_CLOCK_I2C_SCL
        );
    FPIO_I2C_SDA_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(6),
            O => GPIO_I2C_tri_i(6),
            T => GPIO_I2C_tri_t(6),
            IO => B_FPIO_I2C_SDA
        );
    FPIO_I2C_SCL_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(7),
            O => GPIO_I2C_tri_i(7),
            T => GPIO_I2C_tri_t(7),
            IO => B_FPIO_I2C_SCL
        );
    PWR_FAN_I2C_SDA_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(8),
            O => GPIO_I2C_tri_i(8),
            T => GPIO_I2C_tri_t(8),
            IO => B_PWR_FAN_I2C_SDA
        );
    PWR_FAN_I2C_SCL_iobuf : IOBUF
        port map (
            I => GPIO_I2C_tri_o(9),
            O => GPIO_I2C_tri_i(9),
            T => GPIO_I2C_tri_t(9),
            IO => B_PWR_FAN_I2C_SCL
        );
    FAN_FAULT_iobuf : IOBUF
        port map (
            I => FAN_FAULT_N_tri_o,
            O => FAN_FAULT_N_tri_i,
            T => FAN_FAULT_N_tri_t,
            IO => B_FAN_FAULT_N
        );
    C_D_EVENT_N_iobuf : IOBUF
        port map (
            I => C_D_EVENT_N_tri_o,
            O => C_D_EVENT_N_tri_i,
            T => C_D_EVENT_N_tri_t,
            IO => C_D_EVENT_N
        );
    F_D_EVENT_N_iobuf : IOBUF
        port map (
            I => F_D_EVENT_N_tri_o,
            O => F_D_EVENT_N_tri_i,
            T => F_D_EVENT_N_tri_t,
            IO => F_D_EVENT_N
        );
    FPIO_1WIRE_iobuf : IOBUF
        port map (
            I => F_FPIO_1WIRE_tri_o,
            O => F_FPIO_1WIRE_tri_i,
            T => F_FPIO_1WIRE_tri_t,
            IO => B_FPIO_1WIRE
        );
    
    -- C2C IBERT
    i_mgt_clk0 : IBUFDS_GTE2
            port map (
            O => GTREFCLK0_I(0),
            CEB => '0',
            I => MGT_REFCLK0_P,
            IB => MGT_REFCLK0_N
            );
        
    i_mgt_clk1 : IBUFDS_GTE2
            port map (
            O => GTREFCLK1_I(0),
            CEB => '0',
            I => MGT_REFCLK1_P,
            IB => MGT_REFCLK1_N
            );
        
    i_ibert : ibert_7series_gtp_0
        port map (
            TXP_O => MGT_TX_P,
            TXN_O => MGT_TX_N,
            RXP_I => MGT_RX_P,
            RXN_I => MGT_RX_N,
            GTREFCLK0_I => GTREFCLK0_I,
            GTREFCLK1_I => GTREFCLK1_I,
            SYSCLK_I => Clk100
        );

    -- NCSI MMI to RMII converter
    i_mii_to_rmii : mii_to_rmii
        port map (
            rmii2phy_tx_en => B_NCSI_TX_EN,
            rmii2phy_txd => B_NCSI_TXD,
            phy2rmii_crs_dv => B_NCSI_CRS_DV,
            phy2rmii_rx_er => B_NCSI_RX_ER,
            phy2rmii_rxd => B_NCSI_RXD,
            ref_clk => B_NCSIC,
            mac2rmii_tx_en => NCSI_MII_tx_en,
            mac2rmii_tx_er => '0',
            mac2rmii_txd => NCSI_MII_txd,
            rmii2mac_col => NCSI_MII_col,
            rmii2mac_crs => NCSI_MII_crs,
            rmii2mac_rx_clk => NCSI_MII_rx_clk,
            rmii2mac_rx_dv => NCSI_MII_rx_dv,
            rmii2mac_rx_er => NCSI_MII_rx_er,
            rmii2mac_rxd => NCSI_MII_rxd,
            rmii2mac_tx_clk => NCSI_MII_tx_clk,
            rst_n => NCSI_MII_rst_n
        );

    -------------------
    -- UARTs routing --
    -------------------

    -- USB UART1 directly connected to CPU UART0
    B_UART1_TXD <= B_CUART0_RXD;
    B_CUART0_TXD <= B_UART1_RXD;
    B_UART1_RTS <= B_CUART0_CTS;
    B_CUART0_RTS <= B_UART1_CTS;
    
    -- USB UART3 directly connected to FPGA UART
    B_UART3_TXD <= B_FUART_RXD;
    B_FUART_TXD <= B_UART3_RXD;
    B_UART3_RTS <= B_FUART_CTS;
    B_FUART_RTS <= B_UART3_CTS;

    ----------
    -- GPIO --
    ----------

    -- GPIO port 0 inputs
    GPIO_0_tri_i(0) <= F_SSCONF_INIT_B;
    GPIO_0_tri_i(1) <= F_SSCONF_DONE;
    GPIO_0_tri_i(3) <= C_RESET_OUT_N;
    GPIO_0_tri_i(4) <= B_CLOCK_BLOL;
    GPIO_0_tri_i(5) <= B_CLOCK_CLOL;
    GPIO_0_tri_i(6) <= B_CLOCK_FLOL;
    GPIO_0_tri_i(8) <= B_USB2_OC_N;
    GPIO_0_tri_i(23 downto 16) <= B_USERIO_SW_N(8 downto 1);
    GPIO_0_tri_i(24) <= B_PWR_ALERT_N;
    GPIO_0_tri_i(25) <= B_SEQ_ALERT_N;
    GPIO_0_tri_i(26) <= B_PSU_ALERT_N;
    GPIO_0_tri_i(27) <= B_FAN_ALERT_N;

    -- GPIO port 0 outputs
    F_SSCONF_PROG_B <= GPIO_0_tri_o(2);
    B_PSUP_ON <= GPIO_0_tri_o(9);

    -- GPIO port 0 tri
    C_D_EVENT_N_tri_i <= GPIO_0_tri_i(10);
    C_D_EVENT_N_tri_o <= GPIO_0_tri_o(10);
    C_D_EVENT_N_tri_t <= GPIO_0_tri_t(10);
    F_D_EVENT_N_tri_i <= GPIO_0_tri_i(11);
    F_D_EVENT_N_tri_o <= GPIO_0_tri_o(11);
    F_D_EVENT_N_tri_t <= GPIO_0_tri_t(11);
    FAN_FAULT_N_tri_i <= GPIO_0_tri_i(28);
    FAN_FAULT_N_tri_o <= GPIO_0_tri_o(28);
    FAN_FAULT_N_tri_t <= GPIO_0_tri_t(28);
    

    -- GPIO port 1 inputs
    GPIO_1_tri_i(3) <= B_FPIO_PWR_SW_N;
    GPIO_1_tri_i(4) <= B_FPIO_RST_SW_N;
    GPIO_1_tri_i(5) <= B_FPIO_SID_SW_N;
    GPIO_1_tri_i(7) <= B_FPIO_NMI_SW_N;
    GPIO_1_tri_i(12) <= B_FPIO_CI_SW_N;

    -- GPIO port 1 outputs
    B_SPI_SEL_N <= GPIO_1_tri_o(0);
    B_FPIO_PWR_LED <= GPIO_1_tri_o(2);
    B_FPIO_SID_LED <= GPIO_1_tri_o(8);
    B_FPIO_F1_LED <= GPIO_1_tri_o(9);
    B_FPIO_F2_LED <= GPIO_1_tri_o(10);
    B_FPIO_NIC1_LED <= GPIO_1_tri_o(11);
    B_FPIO_NIC2_LED <= GPIO_1_tri_o(13);
    B_USERIO_LED(8 downto 1) <= GPIO_1_tri_o(23 downto 16);
    C_RESET_N <= GPIO_1_tri_o(24);
    C_PLL_DCOK <= GPIO_1_tri_o(25);
    B_OCI2_LNK1 <= GPIO_1_tri_o(26);
    B_OCI3_LNK1 <= GPIO_1_tri_o(27);
    B_C2C_NMI <= GPIO_1_tri_o(28);
    B_FMCC_SEL <= GPIO_1_tri_o(29);
    B_FAN_RESET_N <= GPIO_1_tri_o(30);
    B_USB2_EN <= GPIO_1_tri_o(31);

    -- GPIO port 1 tri
    F_FPIO_1WIRE_tri_i <= GPIO_1_tri_i(6);
    F_FPIO_1WIRE_tri_o <= GPIO_1_tri_o(6);
    F_FPIO_1WIRE_tri_t <= GPIO_1_tri_t(6);
end rtl;
