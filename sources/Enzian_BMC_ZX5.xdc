# Copyright (c) 2023, ETH Zurich.
# All rights reserved.
#
# This file is distributed under the terms in the attached LICENSE file.
# If you do not find this file, copies can be found by writing to:
# ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
#

# The device in the Mercury ZX5 module used in the Enzian BMC (Xilinx
# xc7z015clg485-2)has 4 IO banks pinned out, all of which have IO voltage set
# by the baseboard, plus fixed and multiplexed IO (MIO) pins.
#  * Bank 0   - VCCIO 3.30V
#  * Bank 13  - VCCIO 1.80V
#  * Bank 34  - VCCIO 3.30V
#  * Bank 35  - VCCIO 3.30V
#  * PS MIO 0 - VCCIO 3.30V
#  * PS MIO 1 - VCCIO 3.30V
#  * PS DDR   - VCCIO 1.35V VREF 0.68V

# -----------------------------------------------------------------------------
# Bank 13 (1.8V)
# -----------------------------------------------------------------------------

# XCVU9P serial configuration interface.
set_property PACKAGE_PIN    AB12 [get_ports F_SSCONF_CLK]
set_property IOSTANDARD LVCMOS18 [get_ports F_SSCONF_CLK]
set_property PACKAGE_PIN     T16 [get_ports F_SSCONF_DATA]
set_property IOSTANDARD LVCMOS18 [get_ports F_SSCONF_DATA]
set_property PACKAGE_PIN     V13 [get_ports F_SSCONF_DONE]
set_property IOSTANDARD LVCMOS18 [get_ports F_SSCONF_DONE]
set_property PACKAGE_PIN     V14 [get_ports F_SSCONF_INIT_B]
set_property IOSTANDARD LVCMOS18 [get_ports F_SSCONF_INIT_B]
set_property PACKAGE_PIN     V15 [get_ports F_SSCONF_PROG_B]
set_property IOSTANDARD LVCMOS18 [get_ports F_SSCONF_PROG_B]

# CPU DRAM voltage control (parallel VID)
set_property PACKAGE_PIN     Y17 [get_ports {B_CDV_VID[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[7]}]
set_property PACKAGE_PIN     W17 [get_ports {B_CDV_VID[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[6]}]
set_property PACKAGE_PIN     W16 [get_ports {B_CDV_VID[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[5]}]
set_property PACKAGE_PIN     V16 [get_ports {B_CDV_VID[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[4]}]
set_property PACKAGE_PIN     U16 [get_ports {B_CDV_VID[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[3]}]
set_property PACKAGE_PIN     Y13 [get_ports {B_CDV_VID[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[2]}]
set_property PACKAGE_PIN     Y12 [get_ports {B_CDV_VID[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[1]}]
set_property PACKAGE_PIN    AA12 [get_ports {B_CDV_VID[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_CDV_VID[0]}]

# FPGA (XCVU9P) DRAM voltage control (parallel VID)
set_property PACKAGE_PIN    AB11 [get_ports {B_FDV_VID[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[7]}]
set_property PACKAGE_PIN    AA11 [get_ports {B_FDV_VID[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[6]}]
set_property PACKAGE_PIN    AB14 [get_ports {B_FDV_VID[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[5]}]
set_property PACKAGE_PIN    AB13 [get_ports {B_FDV_VID[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[4]}]
set_property PACKAGE_PIN    AA15 [get_ports {B_FDV_VID[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[3]}]
set_property PACKAGE_PIN     U14 [get_ports {B_FDV_VID[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[2]}]
set_property PACKAGE_PIN     U13 [get_ports {B_FDV_VID[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[1]}]
set_property PACKAGE_PIN     U11 [get_ports {B_FDV_VID[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_FDV_VID[0]}]

# BMC user IO DIP switches on baseboard
set_property PACKAGE_PIN    AA14 [get_ports {B_USERIO_SW_N[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[8]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[8]}]
set_property PACKAGE_PIN     Y15 [get_ports {B_USERIO_SW_N[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[7]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[7]}]
set_property PACKAGE_PIN     Y14 [get_ports {B_USERIO_SW_N[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[6]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[6]}]
set_property PACKAGE_PIN     Y19 [get_ports {B_USERIO_SW_N[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[5]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[5]}]
set_property PACKAGE_PIN     Y18 [get_ports {B_USERIO_SW_N[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[4]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[4]}]
set_property PACKAGE_PIN    AA17 [get_ports {B_USERIO_SW_N[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[3]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[3]}]
set_property PACKAGE_PIN    AA16 [get_ports {B_USERIO_SW_N[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[2]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[2]}]
set_property PACKAGE_PIN    AB22 [get_ports {B_USERIO_SW_N[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_SW_N[1]}]
set_property PULLUP         true [get_ports {B_USERIO_SW_N[1]}]

# BMC user LEDs on baseboard
set_property PACKAGE_PIN     U12 [get_ports {B_USERIO_LED[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[8]}]
set_property PACKAGE_PIN    AB21 [get_ports {B_USERIO_LED[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[7]}]
set_property PACKAGE_PIN    AB19 [get_ports {B_USERIO_LED[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[6]}]
set_property PACKAGE_PIN    AB18 [get_ports {B_USERIO_LED[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[5]}]
set_property PACKAGE_PIN    AB17 [get_ports {B_USERIO_LED[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[4]}]
set_property PACKAGE_PIN    AB16 [get_ports {B_USERIO_LED[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[3]}]
set_property PACKAGE_PIN    AA20 [get_ports {B_USERIO_LED[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[2]}]
set_property PACKAGE_PIN    AA19 [get_ports {B_USERIO_LED[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {B_USERIO_LED[1]}]

# Interrupt pin from XCVU9P
set_property PACKAGE_PIN     W15 [get_ports B_C2C_NMI]
set_property IOSTANDARD LVCMOS18 [get_ports B_C2C_NMI]

# UART from XCVU9P
set_property PACKAGE_PIN     W12 [get_ports B_FUART_CTS]
set_property IOSTANDARD LVCMOS18 [get_ports B_FUART_CTS]
set_property PACKAGE_PIN     W13 [get_ports B_FUART_RTS]
set_property IOSTANDARD LVCMOS18 [get_ports B_FUART_RTS]
set_property PACKAGE_PIN     V11 [get_ports B_FUART_RXD]
set_property IOSTANDARD LVCMOS18 [get_ports B_FUART_RXD]
set_property PACKAGE_PIN     W11 [get_ports B_FUART_TXD]
set_property IOSTANDARD LVCMOS18 [get_ports B_FUART_TXD]

# -----------------------------------------------------------------------------
# Bank 34 (3.3V)
# -----------------------------------------------------------------------------

# Power sequencer (isppac) I2C bus
set_property PACKAGE_PIN      J8 [get_ports B_SEQ_I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports B_SEQ_I2C_SDA]
set_property PULLUP         true [get_ports B_SEQ_I2C_SDA]
set_property PACKAGE_PIN      K2 [get_ports B_SEQ_I2C_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports B_SEQ_I2C_SCL]
set_property PULLUP         true [get_ports B_SEQ_I2C_SCL]

# Power regulator I2C bus alert signal
set_property PACKAGE_PIN      M2 [get_ports B_PWR_ALERT_N]
set_property PULLUP		    true [get_ports B_PWR_ALERT_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_PWR_ALERT_N]

# On Module I2C bus (real-time clock, EEPROM)
set_property PACKAGE_PIN      R8 [get_ports ZX5_I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports ZX5_I2C_SDA]
set_property PACKAGE_PIN      H8 [get_ports ZX5_I2C_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports ZX5_I2C_SCL]

# CPU UARTs
set_property PACKAGE_PIN      R4 [get_ports B_CUART0_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART0_RXD]
set_property PACKAGE_PIN      N4 [get_ports B_CUART0_TXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART0_TXD]
set_property PACKAGE_PIN      M3 [get_ports B_CUART0_RTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART0_RTS]
set_property PACKAGE_PIN      M4 [get_ports B_CUART0_CTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART0_CTS]
set_property PACKAGE_PIN      P5 [get_ports B_CUART1_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART1_RXD]
set_property PACKAGE_PIN      N6 [get_ports B_CUART1_TXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART1_TXD]
set_property PACKAGE_PIN      P6 [get_ports B_CUART1_RTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART1_RTS]
set_property PACKAGE_PIN      N3 [get_ports B_CUART1_CTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_CUART1_CTS]

# USB JTAG UARTs
set_property PACKAGE_PIN      J1 [get_ports B_UART0_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART0_RXD]
set_property PACKAGE_PIN      J5 [get_ports B_UART0_TXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART0_TXD]
set_property PACKAGE_PIN      K5 [get_ports B_UART0_RTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART0_RTS]
set_property PACKAGE_PIN      M8 [get_ports B_UART0_CTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART0_CTS]
set_property PACKAGE_PIN      M7 [get_ports B_UART1_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART1_RXD]
set_property PACKAGE_PIN      N8 [get_ports B_UART1_TXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART1_TXD]
set_property PACKAGE_PIN      P8 [get_ports B_UART1_RTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART1_RTS]
set_property PACKAGE_PIN      L6 [get_ports B_UART1_CTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART1_CTS]
set_property PACKAGE_PIN      M6 [get_ports B_UART2_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART2_RXD]
set_property PACKAGE_PIN      K7 [get_ports B_UART2_TXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART2_TXD]
set_property PACKAGE_PIN      L7 [get_ports B_UART2_RTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART2_RTS]
set_property PACKAGE_PIN      J7 [get_ports B_UART2_CTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART2_CTS]
set_property PACKAGE_PIN      J6 [get_ports B_UART3_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART3_RXD]
set_property PACKAGE_PIN      K8 [get_ports B_UART3_TXD]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART3_TXD]
set_property PACKAGE_PIN      J2 [get_ports B_UART3_RTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART3_RTS]
set_property PACKAGE_PIN      J3 [get_ports B_UART3_CTS]
set_property IOSTANDARD LVCMOS33 [get_ports B_UART3_CTS]

# CPU pinstraps and reset control
set_property PACKAGE_PIN      L1 [get_ports B_OCI2_LNK1]
set_property PACKAGE_PIN      K4 [get_ports B_OCI3_LNK1]
set_property IOSTANDARD LVCMOS33 [get_ports B_OCI2_LNK1]
set_property IOSTANDARD LVCMOS33 [get_ports B_OCI3_LNK1]
set_property PACKAGE_PIN      M1 [get_ports C_PLL_DCOK]
set_property IOSTANDARD LVCMOS33 [get_ports C_PLL_DCOK]
set_property PACKAGE_PIN      N1 [get_ports C_RESET_OUT_N]
set_property IOSTANDARD LVCMOS33 [get_ports C_RESET_OUT_N]
set_property PACKAGE_PIN      R2 [get_ports C_RESET_N]
set_property IOSTANDARD LVCMOS33 [get_ports C_RESET_N]

# FMC clock source selector
set_property PACKAGE_PIN      L2 [get_ports B_FMCC_SEL]
set_property IOSTANDARD LVCMOS33 [get_ports B_FMCC_SEL]

# CPU NCSI (Ethernet sideband interface)
set_property PACKAGE_PIN      R5 [get_ports {B_NCSI_RXD[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {B_NCSI_RXD[0]}]
set_property PACKAGE_PIN      P7 [get_ports {B_NCSI_RXD[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {B_NCSI_RXD[1]}]
set_property PACKAGE_PIN      R7 [get_ports {B_NCSI_TXD[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {B_NCSI_TXD[0]}]
set_property PACKAGE_PIN      K3 [get_ports {B_NCSI_TXD[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {B_NCSI_TXD[1]}]
set_property PACKAGE_PIN      N5 [get_ports B_NCSI_RX_ER]
set_property IOSTANDARD LVCMOS33 [get_ports B_NCSI_RX_ER]
set_property PACKAGE_PIN      R3 [get_ports B_NCSI_TX_EN]
set_property IOSTANDARD LVCMOS33 [get_ports B_NCSI_TX_EN]
set_property PACKAGE_PIN      P3 [get_ports B_NCSI_CRS_DV]
set_property IOSTANDARD LVCMOS33 [get_ports B_NCSI_CRS_DV]
set_property PACKAGE_PIN      D5 [get_ports B_NCSIC]
set_property IOSTANDARD LVCMOS33 [get_ports B_NCSIC]

# USB2 VBUS enable and overcurrent detect
set_property PACKAGE_PIN      T1 [get_ports B_USB2_EN]
set_property IOSTANDARD LVCMOS33 [get_ports B_USB2_EN]
set_property PACKAGE_PIN      T2 [get_ports B_USB2_OC_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_USB2_OC_N]

# CPU and FPGA DRAM ALERT signals
set_property PACKAGE_PIN      L5 [get_ports C_D_EVENT_N]
set_property IOSTANDARD LVCMOS33 [get_ports C_D_EVENT_N]
set_property PACKAGE_PIN      L4 [get_ports F_D_EVENT_N]
set_property IOSTANDARD LVCMOS33 [get_ports F_D_EVENT_N]

# -----------------------------------------------------------------------------
# Bank 35 (3.3V)
# -----------------------------------------------------------------------------

# LED on ZX5 module
set_property PACKAGE_PIN      H5 [get_ports ZX5_LED0_N]
set_property IOSTANDARD LVCMOS33 [get_ports ZX5_LED0_N]

# Power + Fan I2C bus
set_property PACKAGE_PIN      E7 [get_ports B_PWR_FAN_I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports B_PWR_FAN_I2C_SDA]
set_property PULLUP         true [get_ports B_PWR_FAN_I2C_SDA]
set_property PACKAGE_PIN      F7 [get_ports B_PWR_FAN_I2C_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports B_PWR_FAN_I2C_SCL]
set_property PULLUP         true [get_ports B_PWR_FAN_I2C_SCL]
set_property PACKAGE_PIN      D7 [get_ports B_FAN_ALERT_N]
set_property PULLUP		    true [get_ports B_FAN_ALERT_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FAN_ALERT_N]
set_property PACKAGE_PIN      D6 [get_ports B_FAN_FAULT_N]
set_property PULLUP		    true [get_ports B_FAN_FAULT_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FAN_FAULT_N]
set_property PACKAGE_PIN      D8 [get_ports B_FAN_RESET_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FAN_RESET_N]

# Clock generator I2C bus
set_property PACKAGE_PIN      C3 [get_ports B_CLOCK_I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports B_CLOCK_I2C_SDA]
set_property PULLUP         true [get_ports B_CLOCK_I2C_SDA]
set_property PACKAGE_PIN      D3 [get_ports B_CLOCK_I2C_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports B_CLOCK_I2C_SCL]
set_property PULLUP         true [get_ports B_CLOCK_I2C_SCL]

# Clock generator Loss-Of-Lock signals
set_property PACKAGE_PIN      B4 [get_ports B_CLOCK_FLOL]
set_property IOSTANDARD LVCMOS33 [get_ports B_CLOCK_FLOL]
set_property PACKAGE_PIN      B3 [get_ports B_CLOCK_BLOL]
set_property IOSTANDARD LVCMOS33 [get_ports B_CLOCK_BLOL]
set_property PACKAGE_PIN      A2 [get_ports B_CLOCK_CLOL]
set_property IOSTANDARD LVCMOS33 [get_ports B_CLOCK_CLOL]

# ALERT signal for sequencer I2C bus
set_property PACKAGE_PIN      C5 [get_ports B_SEQ_ALERT_N]
set_property PULLUP		    true [get_ports B_SEQ_ALERT_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_SEQ_ALERT_N]

# Front-panel IO
set_property PACKAGE_PIN      B7 [get_ports B_FPIO_PWR_LED]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_PWR_LED]
set_property PACKAGE_PIN      G6 [get_ports B_FPIO_PWR_SW_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_PWR_SW_N]
set_property PULLUP         true [get_ports B_FPIO_PWR_SW_N]
set_property PACKAGE_PIN      F5 [get_ports B_FPIO_RST_SW_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_RST_SW_N]
set_property PULLUP         true [get_ports B_FPIO_RST_SW_N]
set_property PACKAGE_PIN      E5 [get_ports B_FPIO_SID_SW_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_SID_SW_N]
set_property PULLUP         true [get_ports B_FPIO_SID_SW_N]
set_property PACKAGE_PIN      B8 [get_ports B_FPIO_1WIRE]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_1WIRE]
set_property PACKAGE_PIN      F6 [get_ports B_FPIO_NMI_SW_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_NMI_SW_N]
set_property PULLUP         true [get_ports B_FPIO_NMI_SW_N]
set_property PACKAGE_PIN      A7 [get_ports B_FPIO_SID_LED]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_SID_LED]
set_property PACKAGE_PIN      A6 [get_ports B_FPIO_F1_LED]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_F1_LED]
set_property PACKAGE_PIN      A5 [get_ports B_FPIO_F2_LED]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_F2_LED]
set_property PACKAGE_PIN      A4 [get_ports B_FPIO_NIC1_LED]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_NIC1_LED]
set_property PACKAGE_PIN      H1 [get_ports B_FPIO_I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_I2C_SDA]
set_property PULLUP         true [get_ports B_FPIO_I2C_SDA]
set_property PACKAGE_PIN      G1 [get_ports B_FPIO_I2C_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_I2C_SCL]
set_property PULLUP         true [get_ports B_FPIO_I2C_SCL]
set_property PACKAGE_PIN      C8 [get_ports B_FPIO_CI_SW_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_CI_SW_N]
set_property PULLUP         true [get_ports B_FPIO_CI_SW_N]
set_property PACKAGE_PIN      C6 [get_ports B_FPIO_NIC2_LED]
set_property IOSTANDARD LVCMOS33 [get_ports B_FPIO_NIC2_LED]

# CPU SPI boot flash
set_property PACKAGE_PIN      D1 [get_ports B_SPI_SCK]
set_property IOSTANDARD LVCMOS33 [get_ports B_SPI_SCK]
set_property PACKAGE_PIN      C1 [get_ports B_SPI_SEL_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_SPI_SEL_N]
set_property PACKAGE_PIN      E2 [get_ports B_SPI_MISO]
set_property IOSTANDARD LVCMOS33 [get_ports B_SPI_MISO]
set_property PACKAGE_PIN      D2 [get_ports B_SPI_MOSI]
set_property IOSTANDARD LVCMOS33 [get_ports B_SPI_MOSI]
set_property PACKAGE_PIN      B1 [get_ports B_SPI_CS_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_SPI_CS_N]

# PSU I2C bus
set_property PACKAGE_PIN      G8 [get_ports B_PSU_I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports B_PSU_I2C_SDA]
set_property PULLUP         true [get_ports B_PSU_I2C_SDA]
set_property PACKAGE_PIN      G7 [get_ports B_PSU_I2C_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports B_PSU_I2C_SCL]
set_property PULLUP         true [get_ports B_PSU_I2C_SCL]
set_property PACKAGE_PIN      E8 [get_ports B_PSU_ALERT_N]
set_property PULLUP		    true [get_ports B_PSU_ALERT_N]
set_property IOSTANDARD LVCMOS33 [get_ports B_PSU_ALERT_N]

# PSU enable
set_property PACKAGE_PIN      G3 [get_ports B_PSUP_ON]
set_property IOSTANDARD LVCMOS33 [get_ports B_PSUP_ON]

# Suppress some Vivado warnings
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN2.DVD_FF]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN2.RER_FF]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN2.TEN_FF]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[0].RX_FF_I]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[0].TX_FF_I]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[1].RX_FF_I]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[1].TX_FF_I]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[2].RX_FF_I]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[2].TX_FF_I]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[3].RX_FF_I]
set_property IOB FALSE [get_cells i_core_block/axi_ethernetlite_0/U0/IOFFS_GEN[3].TX_FF_I]
